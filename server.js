const path = require('path')
const express = require('express')
const cors = require('cors')
const history = require('connect-history-api-fallback')
const app = express()
const port = process.env.PORT || 3011
const publicPath = path.resolve(__dirname, './dist')
const staticConf = { maxAge: '1y', etag: false }

app.use(cors())
app.use(express.static(publicPath, staticConf))
app.use(history())
app.use(express.static(publicPath, staticConf))

app.get('/', function (req, res) {
  res.render(path.join(__dirname, 'index.html'))
})

app.listen(port, () => {
  console.log(`App running on port ${port}!`)
})
